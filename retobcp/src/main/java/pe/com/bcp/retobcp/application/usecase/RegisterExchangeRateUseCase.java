package pe.com.bcp.retobcp.application.usecase;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import pe.com.bcp.retobcp.adapter.data.model.ExchangeRateModel;
import pe.com.bcp.retobcp.application.exception.DuplicateRateException;
import pe.com.bcp.retobcp.application.port.in.RegisterExchangeRateCommand;
import pe.com.bcp.retobcp.application.port.out.ExchangeRateRepository;
import pe.com.bcp.retobcp.domain.ExchangeRate;
import pe.com.bcp.retobcp.utils.SessionUtils;

import java.time.LocalDateTime;

@Service
@Slf4j
public class RegisterExchangeRateUseCase implements RegisterExchangeRateCommand {
    private ExchangeRateRepository repository;

    public RegisterExchangeRateUseCase(ExchangeRateRepository repository) {
        this.repository = repository;
    }

    @Override
    public ExchangeRate execute(Command command) {
        ExchangeRateModel exchangeRate = ExchangeRateModel.builder()
                .description(command.getDescription())
                .currencySource(command.getCurrencySource().name())
                .currencyDestiny(command.getCurrencyDestiny().name())
                .rateType(command.getRateType().name())
                .rateValue(command.getRateValue())
                .createUser(SessionUtils.geUserName())
                .createDate(LocalDateTime.now())
                .build();
        ExchangeRate exchangeRateResponse;
        try {
            exchangeRateResponse = this.repository.save(exchangeRate).toDomain();
        } catch (DataIntegrityViolationException exception) {
            log.error("Ocurrió un error", exception);
            throw new DuplicateRateException("Ya existe el rate");
        }
        return exchangeRateResponse;
    }

}
