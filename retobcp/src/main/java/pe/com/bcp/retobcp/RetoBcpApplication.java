package pe.com.bcp.retobcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetoBcpApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetoBcpApplication.class, args);
    }

}
