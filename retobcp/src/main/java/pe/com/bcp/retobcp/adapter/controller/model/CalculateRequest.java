package pe.com.bcp.retobcp.adapter.controller.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;
import pe.com.bcp.retobcp.domain.CurrencyType;
import pe.com.bcp.retobcp.domain.RateType;

import java.math.BigDecimal;

@Data
@Builder
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CalculateRequest {
    private CurrencyType currencySource;
    private CurrencyType currencyDestiny;
    private RateType rateType;
    private BigDecimal amountToCalculate;

}
