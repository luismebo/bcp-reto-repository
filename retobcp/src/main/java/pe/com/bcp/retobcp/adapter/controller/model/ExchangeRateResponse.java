package pe.com.bcp.retobcp.adapter.controller.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;
import pe.com.bcp.retobcp.domain.CurrencyType;
import pe.com.bcp.retobcp.domain.ExchangeRate;
import pe.com.bcp.retobcp.domain.RateType;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ExchangeRateResponse {
    private Long id;
    private String description;
    private CurrencyType currencySource;
    private CurrencyType currencyDestiny;
    private BigDecimal rateValue;
    private RateType rateType;
    private String createUser;
    private LocalDateTime createDate;
    private String updateUser;
    private LocalDateTime lastUpdate;

    public static ExchangeRateResponse fromDomain(ExchangeRate exchangeRate) {
        return ExchangeRateResponse.builder()
                .id(exchangeRate.getId())
                .description(exchangeRate.getDescription())
                .currencySource(exchangeRate.getCurrencySource())
                .currencyDestiny(exchangeRate.getCurrencyDestiny())
                .rateValue(exchangeRate.getRateValue())
                .rateType(exchangeRate.getRateType())
                .createUser(exchangeRate.getCreateUser())
                .createDate(exchangeRate.getCreateDate())
                .updateUser(exchangeRate.getUpdateUser())
                .lastUpdate(exchangeRate.getLastUpdate())
                .build();
    }
}
