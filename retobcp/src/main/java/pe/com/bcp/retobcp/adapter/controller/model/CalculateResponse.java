package pe.com.bcp.retobcp.adapter.controller.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;
import pe.com.bcp.retobcp.domain.CurrencyType;
import pe.com.bcp.retobcp.domain.ExchangeRate;
import pe.com.bcp.retobcp.domain.RateType;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CalculateResponse {
    private Long id;
    private String description;
    private CurrencyType currencySource;
    private CurrencyType currencyDestiny;
    private BigDecimal rateValue;
    private RateType rateType;
    private String createUser;
    private LocalDateTime createDate;
    private String updateUser;
    private LocalDateTime lastUpdate;
    private BigDecimal amountToCalculate;
    private BigDecimal amountCalculated;

    public static CalculateResponse fromDomain(ExchangeRate exchangeRate) {
        return CalculateResponse.builder()
                .id(exchangeRate.getId())
                .description(exchangeRate.getDescription())
                .currencySource(exchangeRate.getCurrencySource())
                .currencyDestiny(exchangeRate.getCurrencyDestiny())
                .rateValue(exchangeRate.getRateValue())
                .rateType(exchangeRate.getRateType())
                .amountToCalculate(exchangeRate.getAmountToCalculate())
                .amountCalculated(exchangeRate.getAmountCalculated())
                .createUser(exchangeRate.getCreateUser())
                .createDate(exchangeRate.getCreateDate())
                .updateUser(exchangeRate.getUpdateUser())
                .lastUpdate(exchangeRate.getLastUpdate())
                .build();
    }
}
