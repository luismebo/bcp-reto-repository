package pe.com.bcp.retobcp.application.usecase;

import org.springframework.stereotype.Service;
import pe.com.bcp.retobcp.application.exception.NotFoundException;
import pe.com.bcp.retobcp.application.port.in.GetExchangeRateQuery;
import pe.com.bcp.retobcp.application.port.out.ExchangeRateRepository;
import pe.com.bcp.retobcp.adapter.data.model.ExchangeRateModel;
import pe.com.bcp.retobcp.domain.ExchangeRate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GetExchangeRateUseCase implements GetExchangeRateQuery {
    private ExchangeRateRepository repository;

    public GetExchangeRateUseCase(ExchangeRateRepository repository) {
        this.repository = repository;
    }

    @Override
    public Flux<ExchangeRate> getExchangeRates() {
        List<ExchangeRate> exchangeRates = this.repository.findAll()
                .stream()
                .map(ExchangeRateModel::toDomain)
                .collect(Collectors.toList());
        return Flux.fromIterable(exchangeRates);
    }

    @Override
    public ExchangeRate getExchangeById(Long id) {
        ExchangeRate exchangeRate = this.repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Rate No encontrado"))
                .toDomain();
        return exchangeRate;
    }
}
