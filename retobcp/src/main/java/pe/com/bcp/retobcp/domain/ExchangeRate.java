package pe.com.bcp.retobcp.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@Value
@AllArgsConstructor
public class ExchangeRate {
    private Long id;
    private String description;
    private CurrencyType currencySource;
    private CurrencyType currencyDestiny;
    private BigDecimal rateValue;
    private RateType rateType;
    private String createUser;
    private LocalDateTime createDate;
    private String updateUser;
    private LocalDateTime lastUpdate;
    @With
    private BigDecimal amountToCalculate;
    @With
    private BigDecimal amountCalculated;

}
