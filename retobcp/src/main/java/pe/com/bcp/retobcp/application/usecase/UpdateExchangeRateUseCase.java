package pe.com.bcp.retobcp.application.usecase;

import org.springframework.stereotype.Service;
import pe.com.bcp.retobcp.application.exception.NotFoundException;
import pe.com.bcp.retobcp.application.port.in.UpdateExchangeRateCommand;
import pe.com.bcp.retobcp.application.port.out.ExchangeRateRepository;
import pe.com.bcp.retobcp.adapter.data.model.ExchangeRateModel;
import pe.com.bcp.retobcp.utils.SessionUtils;

import java.time.LocalDateTime;

@Service
public class UpdateExchangeRateUseCase implements UpdateExchangeRateCommand {
    private ExchangeRateRepository repository;

    public UpdateExchangeRateUseCase(ExchangeRateRepository repository) {
        this.repository = repository;
    }

    @Override
    public void execute(Command command) {
        ExchangeRateModel model = repository.findById(command.getId())
                .orElseThrow(() -> new NotFoundException("Rate no encontrado"));
        model.setRateValue(command.getRateValue());
        model.setUpdateUser(SessionUtils.geUserName());
        model.setLastUpdate(LocalDateTime.now());
        this.repository.save(model);
    }

}
