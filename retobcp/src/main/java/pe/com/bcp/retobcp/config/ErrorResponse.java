package pe.com.bcp.retobcp.config;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ErrorResponse {
    String message;
}
