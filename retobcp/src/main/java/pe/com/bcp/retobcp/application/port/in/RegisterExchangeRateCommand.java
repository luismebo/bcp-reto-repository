package pe.com.bcp.retobcp.application.port.in;

import lombok.Builder;
import lombok.Value;
import pe.com.bcp.retobcp.domain.CurrencyType;
import pe.com.bcp.retobcp.domain.ExchangeRate;
import pe.com.bcp.retobcp.domain.RateType;

import java.math.BigDecimal;

public interface RegisterExchangeRateCommand {

    ExchangeRate execute(Command command);

    @Value
    @Builder
    class Command {
        private String description;
        private CurrencyType currencySource;
        private CurrencyType currencyDestiny;
        private BigDecimal rateValue;
        private RateType rateType;
    }


}
