package pe.com.bcp.retobcp.domain;

import lombok.Data;

@Data
public class UserCredential {
	private String username;
	private String password;
}
