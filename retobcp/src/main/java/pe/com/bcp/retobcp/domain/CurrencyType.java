package pe.com.bcp.retobcp.domain;

public enum CurrencyType {
    USD,
    PEN,
    EUR
}
