package pe.com.bcp.retobcp.application.port.out;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pe.com.bcp.retobcp.adapter.data.model.UserModel;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Long> {

    UserModel findByUsername(String username);
}
