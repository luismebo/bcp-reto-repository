package pe.com.bcp.retobcp.domain;

public enum RateType {
    SELLING,
    BUYING
}
