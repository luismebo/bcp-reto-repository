package pe.com.bcp.retobcp.application.port.out;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.bcp.retobcp.adapter.data.model.ExchangeRateModel;

import java.util.Optional;

@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRateModel, Long> {
    Optional<ExchangeRateModel> findOptionalByCurrencySourceAndCurrencyDestinyAndRateType(String currencySource, String currencyDestiny, String rateType);
}
