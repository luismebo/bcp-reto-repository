package pe.com.bcp.retobcp.adapter.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.com.bcp.retobcp.adapter.controller.model.CalculateRequest;
import pe.com.bcp.retobcp.adapter.controller.model.CalculateResponse;
import pe.com.bcp.retobcp.adapter.controller.model.RegisterExchangeRateRequest;
import pe.com.bcp.retobcp.adapter.controller.model.ExchangeRateResponse;
import pe.com.bcp.retobcp.adapter.controller.model.UpdateExchangeRateRequest;
import pe.com.bcp.retobcp.application.port.in.CalculateAmountCommand;
import pe.com.bcp.retobcp.application.port.in.GetExchangeRateQuery;
import pe.com.bcp.retobcp.application.port.in.RegisterExchangeRateCommand;
import pe.com.bcp.retobcp.application.port.in.UpdateExchangeRateCommand;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/ms-exchange")
public class ExchangeRateController {
    private GetExchangeRateQuery getExchangeRateQuery;
    private CalculateAmountCommand calculateAmountCommand;
    private RegisterExchangeRateCommand registerExchangeRateCommand;
    private UpdateExchangeRateCommand updateExchangeRateCommand;

    public ExchangeRateController(GetExchangeRateQuery getExchangeRateQuery,
                                  CalculateAmountCommand calculateAmountCommand,
                                  RegisterExchangeRateCommand registerExchangeRateCommand,
                                  UpdateExchangeRateCommand updateExchangeRateCommand
    ) {
        this.getExchangeRateQuery = getExchangeRateQuery;
        this.calculateAmountCommand = calculateAmountCommand;
        this.registerExchangeRateCommand = registerExchangeRateCommand;
        this.updateExchangeRateCommand = updateExchangeRateCommand;
    }

    @GetMapping
    public ResponseEntity<Flux<ExchangeRateResponse>> getExchangeRates() {
        Flux<ExchangeRateResponse> response = this.getExchangeRateQuery.getExchangeRates()
                .map(ExchangeRateResponse::fromDomain);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Mono<ExchangeRateResponse>> getExchangeRateById(@PathVariable Long id) {
        ExchangeRateResponse response = ExchangeRateResponse
                .fromDomain(this.getExchangeRateQuery.getExchangeById(id));
        return ResponseEntity.ok(Mono.just(response));
    }

    @PostMapping("/calculation")
    public ResponseEntity<Mono<CalculateResponse>> calculate(@RequestBody CalculateRequest request) {
        CalculateAmountCommand.Command command = CalculateAmountCommand.Command.builder()
                .source(request.getCurrencySource())
                .destiny(request.getCurrencyDestiny())
                .rateType(request.getRateType())
                .amount(request.getAmountToCalculate())
                .build();
        CalculateResponse response = CalculateResponse
                .fromDomain(this.calculateAmountCommand.execute(command));
        return ResponseEntity.ok(Mono.just(response));
    }

    @PostMapping
    public ResponseEntity<ExchangeRateResponse> register(@RequestBody RegisterExchangeRateRequest request) {
        RegisterExchangeRateCommand.Command command = RegisterExchangeRateCommand.Command.builder()
                .currencySource(request.getCurrencySource())
                .currencyDestiny(request.getCurrencyDestiny())
                .rateType(request.getRateType())
                .rateValue(request.getRateValue())
                .description(request.getDescription())
                .build();
        ExchangeRateResponse response = ExchangeRateResponse
                .fromDomain(this.registerExchangeRateCommand.execute(command));
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable Long id,
                                       @RequestBody UpdateExchangeRateRequest request
                                       ) {
        UpdateExchangeRateCommand.Command command = UpdateExchangeRateCommand.Command.builder()
                .id(id)
                .rateValue(request.getRateValue())
                .build();
        this.updateExchangeRateCommand.execute(command);
        return ResponseEntity.noContent().build();
    }
}
