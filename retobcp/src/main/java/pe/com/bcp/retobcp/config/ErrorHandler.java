package pe.com.bcp.retobcp.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pe.com.bcp.retobcp.application.exception.DuplicateRateException;
import pe.com.bcp.retobcp.application.exception.NotFoundException;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorResponse> handle(NotFoundException exception) {
        ErrorResponse errorResponse = ErrorResponse.builder().message(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DuplicateRateException.class)
    public ResponseEntity<ErrorResponse> handle(DuplicateRateException exception) {
        ErrorResponse errorResponse = ErrorResponse.builder().message(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
    }

}
