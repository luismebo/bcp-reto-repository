package pe.com.bcp.retobcp.application.port.in;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

public interface UpdateExchangeRateCommand {

    void execute(Command command);

    @Value
    @Builder
    class Command {
        private Long id;
        private BigDecimal rateValue;
    }
}
