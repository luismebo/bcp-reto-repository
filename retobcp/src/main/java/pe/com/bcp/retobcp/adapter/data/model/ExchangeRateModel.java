package pe.com.bcp.retobcp.adapter.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.com.bcp.retobcp.domain.CurrencyType;
import pe.com.bcp.retobcp.domain.ExchangeRate;
import pe.com.bcp.retobcp.domain.RateType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@Entity(name = "ExchangeRate")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRateModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private String currencySource;
    private String currencyDestiny;
    private BigDecimal rateValue;
    private String rateType;
    private String createUser;
    private LocalDateTime createDate;
    private String updateUser;
    private LocalDateTime lastUpdate;

    public ExchangeRate toDomain(){
        return ExchangeRate.builder()
                .id(id)
                .description(description)
                .currencySource(CurrencyType.valueOf(currencySource))
                .currencyDestiny(CurrencyType.valueOf(currencyDestiny))
                .rateValue(rateValue)
                .rateType(RateType.valueOf(rateType))
                .createUser(createUser)
                .createDate(createDate)
                .updateUser(updateUser)
                .lastUpdate(lastUpdate)
                .build();
    }

}
