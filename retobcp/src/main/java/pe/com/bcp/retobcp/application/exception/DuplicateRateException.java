package pe.com.bcp.retobcp.application.exception;

public class DuplicateRateException extends RuntimeException {
    public DuplicateRateException(String msg) {
        super(msg);
    }
}
