package pe.com.bcp.retobcp.application.usecase;

import org.springframework.stereotype.Service;
import pe.com.bcp.retobcp.adapter.data.model.ExchangeRateModel;
import pe.com.bcp.retobcp.application.exception.NotFoundException;
import pe.com.bcp.retobcp.application.port.in.CalculateAmountCommand;
import pe.com.bcp.retobcp.application.port.out.ExchangeRateRepository;
import pe.com.bcp.retobcp.domain.ExchangeRate;
import pe.com.bcp.retobcp.domain.RateType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Service
public class CalculateAmountUseCase implements CalculateAmountCommand {
    private ExchangeRateRepository repository;

    public CalculateAmountUseCase(ExchangeRateRepository repository) {
        this.repository = repository;
    }

    @Override
    public ExchangeRate execute(Command command) {
        Optional<ExchangeRateModel> optionalModel = this.repository.findOptionalByCurrencySourceAndCurrencyDestinyAndRateType
                (command.getSource().name(), command.getDestiny().name(), command.getRateType().name());

        ExchangeRate exchangeRate = optionalModel
                .orElseThrow(() -> new NotFoundException("No existe el rate"))
                .toDomain();

        BigDecimal amountCalculated = RateType.SELLING == exchangeRate.getRateType() ?
                command.getAmount().divide(exchangeRate.getRateValue(), 2, RoundingMode.HALF_UP) :
                command.getAmount().multiply(exchangeRate.getRateValue());

        exchangeRate = exchangeRate.withAmountToCalculate(command.getAmount())
                .withAmountCalculated(amountCalculated);

        return exchangeRate;
    }
}
