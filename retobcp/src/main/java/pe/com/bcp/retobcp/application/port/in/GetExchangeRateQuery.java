package pe.com.bcp.retobcp.application.port.in;

import pe.com.bcp.retobcp.domain.ExchangeRate;
import reactor.core.publisher.Flux;

public interface GetExchangeRateQuery {

    Flux<ExchangeRate> getExchangeRates();

    ExchangeRate getExchangeById(Long id);
}
