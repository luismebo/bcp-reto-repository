package pe.com.bcp.retobcp.application.port.in;

import lombok.Builder;
import lombok.Value;
import pe.com.bcp.retobcp.domain.CurrencyType;
import pe.com.bcp.retobcp.domain.ExchangeRate;
import pe.com.bcp.retobcp.domain.RateType;

import java.math.BigDecimal;

public interface CalculateAmountCommand {

    ExchangeRate execute(Command command);

    @Value
    @Builder
    class Command {
        CurrencyType source;
        CurrencyType destiny;
        RateType rateType;
        BigDecimal amount;
    }
}
