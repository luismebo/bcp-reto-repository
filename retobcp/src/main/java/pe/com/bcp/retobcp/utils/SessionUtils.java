package pe.com.bcp.retobcp.utils;

import lombok.experimental.UtilityClass;
import org.springframework.security.core.context.SecurityContextHolder;

@UtilityClass
public class SessionUtils {

    public String geUserName() {
        return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
